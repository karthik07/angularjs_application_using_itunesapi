# AngularJS Application with mysql and php

## Overview

This application takes the developer through the process of building a web-application using
angular using itunes-api. I have practices angularjs with small applications.

## Workings of the application

- The application filesystem layout structure is based on the [angular-listing-songs-album] project.
- Start with angular application project, to listing songs album, detail pages of that album, search album, and add that album to cart.
- I have used ngRouting for routing, ngCookies for maintain cookies of angular framework.
- 

## use of applications
1. Clone the source.
2. You need some server to run this application.
3. Run your application. 
4. You can use your api to list some product, detail pages, and "Add to cart" funtionality.

