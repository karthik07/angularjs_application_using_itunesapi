var myApp =  angular.module("myApp", [
	'ngRoute',
	'ngCookies',
	'artistcontrollers'
]);

myApp.config(["$routeProvider", function ($routeProvider){
	$routeProvider.

	when('/', {
		templateUrl: 'partial/list-view.html',
		controller: 'ListController'
	}).
	when('/details/:itemId', {
		templateUrl: 'partial/details.html',
		controller: 'DetailController'
	}).
	when('/Cart', {
		templateUrl: 'partial/cart.html',
		controller: 'CartController'
	}).
	otherwise({
		redirectTo : '/',
	})
}]);